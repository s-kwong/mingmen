<?php

// load library
require 'php-excel.class.php';
$url = "http://202.175.81.93:65402/WeiXinLottery.asmx";

$data = https_request($url."/BackStage_PrizeManage_UserPrize_GetListByPage?StageID=1&PrizeID=-1&StartTime=&EndTime=&PageSize=999999&PageNum=1");


$jsonData = json_decode($data, true);

$data = array(
        1 => array ('優惠券類型', '領取時間', '領取電話', '領取人姓名', '使用時間', '狀態')
);

//$PrizeArr = array('1'=>'名門XO醬', '2'=>'10000元代金券', '3'=>'500元代金券', '4'=>'200元代金券','5'=>'100元代金券','6'=>'50元代金券','7'=>'20元代金券','8'=>'20元午市代金券','9'=>'夫妻肺片（满300可用）','10'=>'口水雞（满300可用）','11'=>'涼拌鴨胗（满300可用）','12'=>'四川酸辣粉（满100可用）');

foreach ($jsonData['Object']['UserInfo'] as $key => $val) {
	$status = ($val['IsUse'] == 1)?"已使用":"未使用";
	$tmp = array( $val['PrizeName'], Date("Y-m-d H:i:s", strtotime($val['CreateTime'])), $val['Mobile'],  $val['UserName'], Date("Y-m-d H:i:s", strtotime($val['UseTime'])),  $status);
	$data[] = $tmp;
}

// create a simple 2-dimensional array

// generate file (constructor parameters are optional)
$xls = new Excel_XML('UTF-8', false, 'User Info Sheet');
$xls->addArray($data);
$xls->generateXML('PrizeInfo');

function https_request($url)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($curl);
	if (curl_errno($curl)){
		return 'ERROR'.curl_error($curl);
	}
	curl_close($curl);
	return $data;
}
?>