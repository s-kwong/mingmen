<?php

// load library
require 'php-excel.class.php';
$url = "http://202.175.81.93:65402/WeiXinLottery.asmx";

$data = https_request($url."/BackStage_UserManage_WeiXinUser_GetListByPage?GroupID=1&Mobile=&UserName=&PageSize=999999&PageNum=1");


$jsonData = json_decode($data, true);

$data = array(
        1 => array ('用戶姓名', '性別', '生日', '新增時間', '地址')
);
$Sex = array('1'=>'男', '0'=>'女');
foreach ($jsonData['Object']['UserInfo'] as $key => $val) {
	$tmp = array( $val['UserName'], $Sex[$val['Sex']], Date("Y-m-d H:i:s", strtotime($val['Birthday'])), Date("Y-m-d H:i:s", strtotime($val['CreateTime'])), $val['Adress']);
	$data[] = $tmp;
}

// create a simple 2-dimensional array

// generate file (constructor parameters are optional)
$xls = new Excel_XML('UTF-8', false, 'User Info Sheet');
$xls->addArray($data);
$xls->generateXML('UserInfo');


function https_request($url)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($curl);
	if (curl_errno($curl)){
		return 'ERROR'.curl_error($curl);
	}
	curl_close($curl);
	return $data;
}
?>