<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>名門幸運轉盤后台</title>
	<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.datepicker-zh-CN.js"></script>
	<script type="text/javascript" src="../js/layer.js"></script>
  <script type="text/javascript" src="../js/query.js"></script>
  <script type="text/javascript" src="../js/paging.js"></script>

  <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.10.1.css?v=3">
  <link rel="stylesheet" type="text/css" href="../css/vigo.datepicker.css?v=3">
	<link rel="stylesheet" type="text/css" href="../css/admin.css?v=3">
  <link rel="stylesheet" type="text/css" href="../css/paging.css?v=3">
  
</head>
<body>
  <?php include_once('Header.php'); ?>
  <div class="bottomBody">
    <?php include_once('LeftMenu.php'); ?>
    <div class="rightContent">
      <div class="searchBar">
          <div class="searchItem" style="width:316px">優惠券類型：<select id="prizeID" name="prizeID" style="width:180px"><option value="-1">全部</option></select></div>
          <div class="searchItem" style="width:420px">使用時間：<input type='text' name='startTime' id='startTime' style="width:120px">&nbsp;至&nbsp;<input type='text' name='endTime' id='endTime' style="width:120px"></div>
          <input type='button' value='查詢' class="btn" id="searchBtn">
          <input type='button' value='導出Excel' class="btn" onclick="window.open('../PrizeInfoExcel.php')" style="background:green">
      </div>
      <div class="contentTable" cellpadding='0' cellspacing='0'>
        <table id="dataTable">
          <thead>
          <tr>
            <td width="6%">序號</td>
            <td width="12%">優惠券類型</td>
            <td width="12%">領取時間</td>
            <td width="20%">領取電話</td>
            <td width="20%">領取人姓名</td>
            <td width="20%">使用時間</td>
            <td width="20%">狀態</td>
          </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
        <div class="pageDiv">
              <div id="pageTool"></div>
        </div>
      </div>
    </div>
  </div>
</body>
<script type="text/javascript">
$(function(){
  Search(1);

  $('#searchBtn').click(function(){
    Search(1);
  });

    $( "#startTime" ).datepicker({
      inline: true,
      showOtherMonths: true,
      dateFormat:'yy-mm-dd'
    });
    $( "#endTime" ).datepicker({
      inline: true,
      showOtherMonths: true,
      dateFormat:'yy-mm-dd'
    });

     $.ajax({
      type: "POST",
      url: "../ajax.php?act=BackStage_PrizeManage_Prize_GetInfo",
      dataType: "json",
      timeout : 10000,
      success:function(data){
          if(data.IsSuccess == true){
            for(var i = 0; i < data.Object.length; i++){
              var item = data.Object[i];
              //$('#prizeID').append ( '<options value="'++'">'+item.PrizeName+'</option>');
              $('#prizeID').append( $("<option/>", {
                value: item.PrizeID,
                text: item.PrizeName
              }));

            }
          }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          console.log(XMLHttpRequest);
          //document.write(XMLHttpRequest.status);
      }
  });   

});

var Paging =new Paging();
Paging.init({target:$('#pageTool'), pagesize:10,count:10,callback:function(page,size,count){
  console.log(arguments)
  Search(page);
}});

function Search(page){
    $.ajax({
      type: "POST",
      url: "../ajax.php?act=BackStage_PrizeManage_UserPrize_GetListByPage",
      dataType: "json",
      data:{
        PrizeID: $('#prizeID').val(),
        StartTime: $('#startTime').val(),
        EndTime: $('#endTime').val(),
        PageSize : 10,
        PageNum: page
      },
      timeout : 10000,
      success:function(data){
          //console.log(data);
          $('#dataTable tbody').children().remove();
          //$('#pageTool').remove();
          if(data.IsSuccess == true){
            var tr;
            for(var i = 0; i < data.Object.UserInfo.length; i++){
              var item = data.Object.UserInfo[i];

              var ct = new Date(item.CreateTime).Format("yyyy-MM-dd hh:mm:ss");
              var ut = (item.UseTime != null) ? new Date(item.UseTime).Format("yyyy-MM-dd hh:mm:ss"):'';
              var status = (item.IsUse == 1)?'已使用':'未使用';
              tr += '<tr><td>'+item.UserPrizeID+'</td><td>'+item.PrizeName+'</td><td>'+ct+'</td><td>'+item.Mobile+'</td><td>'+item.UserName+'</td><td>'+ut+'</td><td>'+status+'</td></tr>';
            }

            $('#dataTable tbody').append(tr);
            //var pageCount = Math.ceil(data.Object.PageTotal / 2);

            Paging.render({count:data.Object.PageTotal,current:page});

          }else{
            $('#dataTable').append('<tr><td colspan="6">暂时无数据</td></tr>');
          }

      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          console.log(XMLHttpRequest);
          //document.write(XMLHttpRequest.status);
      }
  });
}




Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
  "M+": this.getMonth() + 1, //月份
  "d+": this.getDate(), //日
  "h+": this.getHours(), //小时
  "m+": this.getMinutes(), //分
  "s+": this.getSeconds(), //秒
  "q+": Math.floor((this.getMonth() + 3) / 3), //季度
  "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt))
  fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
  if (new RegExp("(" + k + ")").test(fmt))
  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

</script>

</html>