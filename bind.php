<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>名門萬元大抽獎</title>
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">

    <link rel="stylesheet" href="css/easyform/platform-1.css">
    <link rel="stylesheet" href="js/easyform/easyform.css">
	<link rel="stylesheet" href="css/datetime/normalize3.0.2.min.css" />
	<link rel="stylesheet" href="css/datetime/style.css?v=7" />
	<link href="css/datetime/mobiscroll.css" rel="stylesheet" />
	<link href="css/datetime/mobiscroll_date.css" rel="stylesheet" />

	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script src="js/easyform/easyform.js"></script>
	<script src="js/datetime/mobiscroll_date.js" charset="gb2312"></script> 
	<script src="js/datetime/mobiscroll.js"></script>
    <script type="text/javascript" src="js/layer.js"></script> 

</head>
<body>
    <div class="nook topLeft"><img src='images/nook.png'></div>
    <div class="nook topRight"><img src='images/nook.png'></div>
    <div class="nook bottomLeft"><img src='images/nook.png'></div>
    <div class="nook bottomRight"><img src='images/nook.png'></div>
	<div class="main">
    <div class="topTitle"><span>首次使用請先完善個人信息<br><hr></span> </div>
    <div class="form-div">
        <form id="reg-form" action="" method="post">
            <table>
                <tr><td>*電話</td></tr>
                <tr>
                    <td width='300px' height="65px">

							
                    		<input name="Mobile" type="text" id="Mobile"
                                placeholder="请填写正确的手机号码"  data-easytip="position:bottom;class:easy-red;" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')">
                    </td>
                </tr>
                <tr><td>*姓名</td></tr>
                <tr>
                    <td width='300px' height="65px">
                    	<input name="UserName" type="text" id="UserName" 
                               data-message="请填写正确的姓名"
                               data-easytip="position:bottom;class:easy-red;" placeholder="请填写正确的姓名"></td>
                </tr>
                <tr><td>*性別</td></tr>
                <tr>
                    <td width='300px' height="65px">
						<select name="Sex" id="Sex" style="border: 0;color:#000"><option value='1'>男</option><option value='2'>女</option></select>
                    </td>
                </tr>
                <tr><td>*生日</td></tr>
                <tr>
                    <td width='300px' height="65px">
                    	<input name="Birthday" type="text" id="Birthday" readonly placeholder="请填写您的生日" >
                    </td>
                </tr>
                <tr><td>*地址</td></tr>
                <tr>
                    <td width='300px' height="65px">
                    	<input name="Address" type="text" id="Address" data-message="请填写正确的地址"
                               data-easytip="position:bottom;class:easy-red;" placeholder="请填写正确的地址">
                    </td>
                </tr>
            </table>

            <div class="buttons" style="margin-top: 30px;">
                <input value="提&nbsp;&nbsp;&nbsp;交" type="button" id="submitBtn">
            </div>

            <br class="clear">
        </form>


    </div>
	</div>
</body>
<script type="text/javascript">
$(document).ready(function (){

    $('#submitBtn').click(function(){

        if( $('#Mobile').val() == ""){
            $('#Mobile').easytip().show('手機號碼不能為空');
            return false;

        }else if( $('#UserName').val() == "" ){
            $('#UserName').easytip().show('姓名不能为空');
            return false;
        }else if( $('#Address').val() == "" ){
            $('#Address').easytip().show('地址不能为空');
            return false;
        }else if( $('#Birthday').val() == "" ){
            $('#Birthday').easytip().show('生日不能为空');
            return false;
        }else{
            var cn = /^1\d{10}$/
            var mc = /^((((0?)|((00)?))(((\s){0,2})|([-_－—\s]?)))|([+]?))(853)?([]?)([-_－—\s]?)(28[0-9]{2}|((6|8)[0-9]{3}))[-_－—\s]?[0-9]{4}$/
            var hk = /^((((0?)|((00)?))(((\s){0,2})|([-_－—\s]?)))|(([(]?)[+]?))(852)?([)]?)([-_－—\s]?)((2|3|5|6|9)?([-_－—\s]?)\d{3})(([-_－—\s]?)\d{4})$/
            if ( cn.test( $('#Mobile').val() ) || mc.test( $('#Mobile').val())  || hk.test( $('#Mobile').val()) ) {
                var layerPostIndex = layer.load(0, {shade: [0.3,'#fff'] });
               $.ajax({
                    cache: false,
                    type: "POST",
                    dataType:"json",
                    url:'ajax.php?act=UserManage_WeiXinUser_Update',
                    data:$('#reg-form').serialize(),// 你的formid
                    async: false,
                    error: function(request) {
                        layer.alert('網絡異常，請稍后再試');
                    },
                    success: function(data) {
                        if(data.IsSuccess == true){
                            layer.close(layerPostIndex);
                            layer.alert(data.Decription, function(){
                                window.location.href='coupon.php'; 
                            });
                        }
                    }
                });            

            } else {

                $('#Mobile').easytip().show('請輸入正確的手機號碼');
                return false;
            }
        }

    });
});


$(function () {
	var currYear = (new Date()).getFullYear();	
	var opt={};
	opt.date = {preset : 'date'};
	opt.datetime = {preset : 'datetime'};
	opt.time = {preset : 'time'};
	opt.default = {
		theme: 'android-ics light', //皮肤样式
		display: 'modal', //显示方式 
		mode: 'scroller', //日期选择模式
		dateFormat: 'yyyy-mm-dd',
		lang: 'zh',
		showNow: false,
		startYear: currYear - 50, //开始年份
		endYear: currYear + 10 //结束年份
	};

	$("#Birthday").mobiscroll($.extend(opt['date'], opt['default']));
});
</script>
</html>