<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>名門萬元大抽獎</title>
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/layer.js"></script>
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="rules" id="rules"><img src="images/rules.png?v=2"></div>
	<div class="banner"><img src="images/top.jpg?v=2"></div>
	<div class="lottery" style="height:96.5vw">
		<div class="qrcode" >
	    	<img src="images/qrcode.jpg">
		</div>
	</div>
	<div class="turntableFooter"><img src="images/turntableFooter.jpg"></div>
	<div class="footer">
		<div class="follow" id="follow"><img src="images/followBtn.png"></div>				
	</div>

	<div class="water">
		<img src="images/water.jpg">
	</div>
	<div class="showGoods">
		<div class="giftHeader">
			<img src="images/giftTitle.png">
		</div>
		<table cellspacing="5" cellpadding="0" border='0' class="goodsTable">
            <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/cqxm'">
                        <span class="goodsName">重慶小麵</span>
                        <span class="goodsImg"><img src="images/goods/13.png" ></pan>
                    </div>
                </td>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/djj'">
                        <span class="goodsName">代金券</span>
                        <span class="goodsImg"><img src="images/goods/2.png" ></span>
                    </div>
                </td>
            </tr>
                   <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/fqfp'">
                        <span class="goodsName">夫妻肺片</span>
                        <span class="goodsImg"><img src="images/goods/9.png" ></span>
                    </div>
                </td>
                <td>
                    <div class="goodsContent"  onclick="window.location.href='adv/ksj'">
                        <span class="goodsName">口水雞</span>
                        <span class="goodsImg"><img src="images/goods/10.png" ></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="goodsContent"  onclick="window.location.href='adv/lbyz'">
                        <span class="goodsName">凉拌鴨胗</span>
                        <span class="goodsImg"><img src="images/goods/11.png" ></span>
                    </div>
                </td>
                <td>
                    <div class="goodsContent"  onclick="window.location.href='adv/scslf'">
                        <span class="goodsName">酸辣粉</span>
                        <span class="goodsImg"><img src="images/goods/12.png" ></span>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/xnd'">
                        <span class="goodsName">鮮牛肚</span>
                        <span class="goodsImg"><img src="images/goods/16.png" ></pan>
                    </div>
                </td>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/agsxhnr'">
                        <span class="goodsName" style="line-height:20px">安格斯<br>雪花牛肉</span>
                        <span class="goodsImg"><img src="images/goods/14.png" ></span>
                    </div>
                </td>
            </tr>
                  <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/xlky'">
                        <span class="goodsName">烤魚</span>
                        <span class="goodsImg"><img src="images/goods/15.png" ></pan>
                    </div>
                </td>
                <td>
                </td>
            </tr>
		</table>
	</div>

</body>
<script type="text/javascript">
	$(function(){

		$('#follow').click(function(){
			layer.alert("長按圖中二維碼，關注公眾號即可參與游戲");
		});

	    $('.rules').on('click',function(){
	        var ruleLayerIndex = layer.open({
	          type: 1
	          ,title: '活動規則'
	          ,scrollbar:false
	          ,closeBtn: false
	          ,area: ['90%', '120vw']
	          ,shade: 0.8
	          ,id: 'LAY_rules' //设定一个id，防止重复弹出
	          ,resize: false
	          ,skin: 'rulesSkin'
	          ,btnAlign: 'c'
	          ,moveType: 1 //拖拽模式，0或者1
	          ,content: '<div class="rulesDiv"><div class="rules-content"><ul><li>1、抽獎方式：點擊開始抽獎,如若抽中獎品,可根據禮品兌換時間及使用要求到門店使用；</li><li>2、關注【名門餐飲】微信公眾號，即可參加抽獎；</li><li>3、首次參加有3次抽獎機會；</li><li>4、每日登録微信公眾號增加2次抽獎機會；</li><li>5、分享鏈接即可獲更多機會，每有1位朋友點擊，即可增加1次抽獎機會；</li><li>6、通過活動所中禮品不得轉讓、退款或兌換現金，亦不可與其他優惠同時使用；</li><li>7、每單消費僅限使用一張優惠券，優惠券不能同一單多張使用；</li><li>8、中獎人領獎時必須與我司頒獎嘉賓合照以完成獎品頒發及記錄程式,中獎人有關照片將用於我日後之合法宣傳</li><li>9、名門集團保留随時修改條款及細則的權利,無須預先通知;</li><li>10、活動時間：2017年5月1日-5月15日;</li><li>11、禮品兌換：2017年5月2日-6月15日;</li><li>12、門店地址：澳門氹仔大連街389號寶龍花園第四座地下B鋪;</li><li>13、聯繁電話： (+853)2883 0993;</li><li>14、本次活動最终決定權歸名門集團所有。</li></ul> </div> <div class="closeDiv"><div class="closeBottom"><div class="closeBtn" id="closeBtn">我知道了</div></div> </div> </div>'
	          ,success:function(){
	            $('#closeBtn').click(function(){layer.close(ruleLayerIndex);});
	          }
	        });
			//document.getElementById("layui-layer"+ruleLayerIndex).addEventListener("touchmove", preventBehavior);
			document.getElementById("layui-layer-shade"+ruleLayerIndex).addEventListener("touchmove", preventBehavior);
	    });
	});
function preventBehavior(e) {
    e.preventDefault(); 
}

</script>
</html>