<?php
session_start();
if(!isset($_SESSION['OpenID']) || empty($_SESSION['OpenID'])){
    header("Location:index.php");
}
    require_once("jssdk.php");
    $jssdk = new JSSDK($_SESSION['app']['appid'], $_SESSION['app']['appsecret']);
    //$jssdk = new JSSDK('wxf5df665edaf5484a', '1599a74ce1f16b2e023d58db31aa83e3');
    $signPackage = $jssdk->GetSignPackage();
    /*
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
    */
   
//var_dump($_SESSION);
// var_dump($signPackage);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>名門萬元大抽獎</title>
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/layer.js"></script>
	<script type="text/javascript" src="js/weixin.js?v=5"></script>
	<link rel="stylesheet" type="text/css" href="css/main.css?v=38">
</head>
<body>
	<div class="rules" id="rules"><img src="images/rules.png?v=2"></div>
	<div class="banner"><img src="images/top.jpg?v=2"></div>
	

	<div class="lottery" id="lottery">
		<table id="prize" cellspacing="10" cellpadding="0" class="prize" border='0'>
			<tr class="mes">
				<td class="lottery-unit lottery-unit-0"><img src="images/table/1.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-1"><img src="images/table/2.png"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-2"><img src="images/table/3.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-3"><img src="images/table/4.jpg"><div class="mask"></td>	
			</tr>
			<tr class="mes">
				<td class="lottery-unit lottery-unit-4"><img src="images/table/5.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-5"><img src="images/table/6.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-6"><img src="images/table/7.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-7"><img src="images/table/14.png"><div class="mask"></td>
			</tr>
			<tr class="mes">
				<td class="lottery-unit lottery-unit-8"><img src="images/table/13.png"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-9"><img src="images/table/11.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-10"><img src="images/table/8.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-11"><img src="images/table/15.png"><div class="mask"></td>
			</tr>
			<tr class="mes">
				<td class="lottery-unit lottery-unit-12"><img src="images/table/12.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-13"><img src="images/table/9.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-14"><img src="images/table/7.jpg"><div class="mask"></td>
				<td class="lottery-unit lottery-unit-15"><img src="images/table/10.jpg"><div class="mask"></td>
			</tr>

	     </table>
	</div>
	<div class="turntableFooter"><img src="images/turntableFooter.jpg"></div>
	<div class="footer">
		<div class="gameTimes">遊戲次數：<span id="times">0</span></div>
		<div class="startGame" id="startGame"><img src="images/startGame.png"></div>
		<div class="gift" id="gift"><img src="images/gift.png"></div>					
	</div>

	<div class="water">
		<img src="images/water.jpg">
	</div>
	<div class="showGoods">
		<div class="giftHeader">
			<img src="images/giftTitle.png">
		</div>
		<table cellspacing="5" cellpadding="0" border='0' class="goodsTable">
			<tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/cqxm'">
                        <span class="goodsName">重慶小麵</span>
                        <span class="goodsImg"><img src="images/goods/13.png" ></pan>
                    </div>
                </td>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/djj'">
                        <span class="goodsName">代金券</span>
                        <span class="goodsImg"><img src="images/goods/2.png" ></span>
                    </div>
                </td>
            </tr>
			       <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/fqfp'">
                        <span class="goodsName">夫妻肺片</span>
                        <span class="goodsImg"><img src="images/goods/9.png" ></span>
                    </div>
                </td>
                <td>
                    <div class="goodsContent"  onclick="window.location.href='adv/ksj'">
                        <span class="goodsName">口水雞</span>
                        <span class="goodsImg"><img src="images/goods/10.png" ></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="goodsContent"  onclick="window.location.href='adv/lbyz'">
                        <span class="goodsName">凉拌鴨胗</span>
                        <span class="goodsImg"><img src="images/goods/11.png" ></span>
                    </div>
                </td>
                <td>
                    <div class="goodsContent"  onclick="window.location.href='adv/scslf'">
                        <span class="goodsName">酸辣粉</span>
                        <span class="goodsImg"><img src="images/goods/12.png" ></span>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/xnd'">
                        <span class="goodsName">鮮牛肚</span>
                        <span class="goodsImg"><img src="images/goods/16.png" ></pan>
                    </div>
                </td>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/agsxhnr'">
                        <span class="goodsName" style="line-height:20px">安格斯<br>雪花牛肉</span>
                        <span class="goodsImg"><img src="images/goods/14.png" ></span>
                    </div>
                </td>
            </tr>
                  <tr>
                <td>
                    <div class="goodsContent" onclick="window.location.href='adv/xlky'">
                        <span class="goodsName">烤魚</span>
                        <span class="goodsImg"><img src="images/goods/15.png" ></pan>
                    </div>
                </td>
                <td>
                </td>
            </tr>


		</table>
	</div>
    <input type="hidden" id="Mobile" value="">
</body>
<script type="text/javascript">

$(function(){

    $('.rules').on('click',function(){
        var ruleLayerIndex = layer.open({
          type: 1
          ,title: '活動規則'
          ,scrollbar:false
          ,closeBtn: false
          ,area: ['90%', '120vw']
          ,shade: 0.8
          ,id: 'LAY_rules' //设定一个id，防止重复弹出
          ,resize: false
          ,skin: 'rulesSkin'
          ,btnAlign: 'c'
          ,moveType: 1 //拖拽模式，0或者1
          ,content: '<div class="rulesDiv"><div class="rules-content"><ul><li>1、抽獎方式：點擊開始抽獎,如若抽中獎品,可根據禮品兌換時間及使用要求到門店使用；</li><li>2、關注【名門餐飲】微信公眾號，即可參加抽獎；</li><li>3、首次參加有3次抽獎機會；</li><li>4、每日登録微信公眾號增加2次抽獎機會；</li><li>5、分享鏈接即可獲更多機會，每有1位朋友點擊，即可增加1次抽獎機會；</li><li>6、通過活動所中禮品不得轉讓、退款或兌換現金，亦不可與其他優惠同時使用；</li><li>7、每單消費僅限使用一張優惠券，優惠券不能同一單多張使用；</li><li>8、中獎人領獎時必須與我司頒獎嘉賓合照以完成獎品頒發及記錄程式,中獎人有關照片將用於我日後之合法宣傳;</li><li>9、名門集團保留随時修改條款及細則的權利,無須預先通知;</li><li>10、活動時間：2017年5月1日-5月15日;</li><li>11、禮品兌換：2017年5月2日-6月15日;</li><li>12、門店地址：澳門氹仔大連街389號寶龍花園第四座地下B鋪;</li><li>13、聯繁電話： (+853)2883 0993;</li><li>14、本次活動最终決定權歸名門集團所有。</li></ul> </div> <div class="closeDiv"><div class="closeBottom"><div class="closeBtn" id="closeBtn">我知道了</div></div> </div> </div>'
          ,success:function(){
            $('#closeBtn').click(function(){layer.closeAll();});
            //addScrollListener('LAY_rules','layui-layer2');
          }
        });
		//document.getElementById("layui-layer"+ruleLayerIndex).addEventListener("touchmove", preventBehavior);
		document.getElementById("layui-layer-shade"+ruleLayerIndex).addEventListener("touchmove", preventBehavior);
    });
	
    $('.gift').click(function(){
    	goCoupan();
    });


    <?php if( isset($_SESSION['ShareOpenID']) && !empty($_SESSION['ShareOpenID']) ){ ?>
        $.ajax({
            type: "POST",
            url: "ajax.php?act=UserManage_ClickShareRecord_Insert",
            dataType: "json",
            timeout : 10000,
            success:function(data){
                console.log(data);
                //if(data.Flag == 1){
                    //layer.alert(data.Decription);
                //}
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
                //document.write(XMLHttpRequest.status);
            }
        });
    <?php } ?>





});

 function goCoupan(){
    window.location.href='coupon.php';
 }

</script>

<script type="text/javascript">
var lottery={
    index:-1,    //当前转动到哪个位置，起点位置
    count:16,    //总共有多少个位置
    timer:0,    //setTimeout的ID，用clearTimeout清除
    speed:230,    //初始转动速度
    times:0,    //转动次数
    cycle:30,    //转动基本次数：即至少需要转动多少次再进入抽奖环节
    prize:-1,    //中奖位置
    rand : 0,
    init:function(id){
        if ($("#"+id).find(".lottery-unit").length>0) {
            this.rand = Math.round(this.count*Math.random());
            $lottery = $("#"+id);
            $units = $lottery.find(".lottery-unit");
            this.obj = $lottery;
            this.count = $units.length;
            //$lottery.find(".lottery-unit-"+this.rand).addClass("active");
        };
    },
    roll:function(){
        var index = this.index;
        var count = this.count;
        var lottery = this.obj;

        $(lottery).find(".lottery-unit-"+this.rand).removeClass("active");

        index += 1;
        if (index>count-1) {
            index = 0;
        };

        if(index !== this.prize){
            this.rand  = Math.round(this.count*Math.random());        
        }else{
            this.rand = index;
        }

        $(lottery).find(".lottery-unit-"+ this.rand ).addClass("active");

        this.index = index;

        return false;
    },
    stop:function(index){
        this.prize=index;

        return false;
    }
};

function roll(){
    lottery.times += 1;
    lottery.roll();//转动过程调用的是lottery的roll方法，这里是第一次调用初始化
    if (lottery.times > lottery.cycle+10 && lottery.prize==lottery.index) {
        clearTimeout(lottery.timer);
        lottery.prize=-1;
        lottery.times=0;
        click=false;

        setTimeout(function(){
            var layerOpen = layer.open({
              type: 1
              ,title: false 
              ,scrollbar:false
              ,closeBtn: true
              ,area: ['318px','302px']
              ,shade: 0.8
              ,offset:'20%'
              ,id: 'LAY_err'
              ,resize: false
              ,skin: 'tipSkin'
              ,btnAlign: 'c'
              ,moveType: 1 //拖拽模式，0或者1

              //,content: '<div class="tipContent"><span class="tipGift"><span class="tipImg"><img src="images/goods/14.png"></span></span>  <div class="closeDiv"><div class="lotteryDesc">恭喜你獲得安格斯雪花牛肉（滿6000可用）</div><div class="closeBottom">  <div class="showBtn" id="showBtn">查看禮品</div><div class="useBtn" id="useBtn">我要使用</div></div></div></div>'
              ,content: '<div class="tipContent"><span class="tipGift"><span class="tipImg"><img src="images/goods/'+lottery.imgID+'.png"></span></span>  <div class="closeDiv"><div class="lotteryDesc">'+lottery.info.PrizeDesc+'</div><div class="closeBottom">  <div class="showBtn" id="showBtn">查看禮品</div><div class="useBtn" id="useBtn">我要使用</div></div></div></div>'
              ,success:function(){

                $('#showBtn').click(function(){
                    goCoupan();
                });
                $('#useBtn').click(function(){
                    if($('#Mobile').val() == ''){
                        window.location.href='bind.php';
                    }else{
                        layer.close(layerOpen);
                        var isUserId = layer.open({
                          type: 1
                          ,title: false 
                          ,scrollbar:false
                          ,closeBtn: true
                          ,area: ['318px','302px']
                          ,shade: 0.8
                          ,offset:'20%'
                          ,id: 'LAY_use'
                          ,resize: false
                          ,skin: 'tipSkin'
                          ,moveType: 1 //拖拽模式，0或者1
                          ,content: '<div class="tipContent"><span class="tipGift"><span class="tipImg"><img src="images/goods/'+lottery.imgID+'.png"></span></span>  <div class="closeDiv"><div class="useTip">請在餐廳工作人員下點擊"我要使用", 使用后禮券不可再次使用.</div><div class="closeBottom"> <div class="useBtnC" id="useBtnC">我要使用</div></div></div></div>'
                          ,success:function(){
                            $('#useBtnC').click(function(){
                                layer.close(isUserId);
                                var geturl = "ajax.php?act=PrizeManage_UserPrize_UpdateIsUse";
                                 $.ajax({
                                 　 url:geturl,
                                    type:"POST",
                                    dataType:"json",
                                    data:{UserPrizeID:lottery.info.UserPrizeID},
                                    success:function(data){
                                        if(data.IsSuccess == true){
                                            layer.alert("使用成功");
                                        }else{
                                            layer.alert("使用失败");
                                        }
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                        layer.open({
                                          type: 1
                                          ,title: false 
                                          ,scrollbar:false
                                          ,closeBtn: true
                                          ,area: ['318px','302px']
                                          ,shade: 0.8
                                          ,offset:'20%'
                                          ,id: 'LAY_err'
                                          ,resize: false
                                          ,skin: 'tipSkin'
                                          ,btnAlign: 'c'
                                          ,moveType: 1 //拖拽模式，0或者1
                                          ,content: '<div class="tipContent"><span class="tipSpan"><img src="images/err.jpg"></span> <div class="closeDiv"><span class="reload">請刷新重試</span><div class="closeBottom"> <div class="closeBtn" id="closeBtn">重试</div></div></div></div>'
                                          ,success:function(){
                                            $('#closeBtn').click(function(){window.location.reload();});
                                          }
                                        });
                                    }
                                });

                            });//btnC
                            }
                        });//open
                        
                    }//else
                });//usebtn
              }
            });
        },2000);


    }else{
        if (lottery.times<lottery.cycle) {
            lottery.speed -= 10;
        }else if(lottery.times==lottery.cycle) {
            //这是随机抽奖
            //var index = Math.random()*(lottery.count)|0;

        }else{
            if (lottery.times > lottery.cycle+10 && ((lottery.prize==0 && lottery.index==15) || lottery.prize==lottery.index+1)) {
                lottery.speed += 110;
            }else{
                lottery.speed += 20;
            }
        }
        if (lottery.speed<40) {
            lottery.speed=40;
        };
        //console.log( 'times:' + lottery.times+',cycle'+lottery.cycle +', speed:'+lottery.speed+', prize:'+lottery.prize+', rand:'+lottery.rand+', index:'+lottery.index);
        lottery.timer = setTimeout(roll,lottery.speed);//循环调用
    }
    return false;
}

var click=false;

window.onload = function(){
    
    lottery.init('lottery');

    $("#startGame").click(function(){
        if(click == true){
            return;
        }

        if(Number($('#times').text()) == 0){
            var shareLayerID = layer.open({
              type: 1
              ,area: ['100%','100%']
              ,offset:0
              ,title:false
              ,closeBtn:false
              ,id: 'LAY_notime'
              ,content: '<img src="images/notime.jpg"><div class="notimeBtnDiv"><div class="notimeBtn" id="notimeBtn">确&nbsp;定</div></div>'
              ,success:function(){
                $('#notimeBtn').click(function(){layer.close(shareLayerID)});
              }
            });
            return;
        }


        var layerLoadGameResultIndex = layer.load(0, {shade: [0.3,'#fff'] });

        //向后端接口发请求返回中奖结果
        var geturl = "ajax.php?act=PrizeManage_UserPrize_Insert";
         $.ajax({
         　 url:geturl,
            type:"POST",
            dataType:"json",
            success:function(data){
                console.log(data);
                if(data.IsSuccess === true ){
                    layer.close(layerLoadGameResultIndex);

                    loadGameTimes();

                    var PrizeID    = data.Object[0].PrizeID;
                    lottery.info = data.Object[0];

                    var rd = parseInt(Math.random()*10);
                    
                    lottery.prize   = PrizeID;

                    switch(data.Object[0].PrizeID){
                        case 2:
                            lottery.prize = 9;
                            lottery.imgID = 2;
                            break;
                        case 3:
                            lottery.prize = 7;
                            lottery.imgID = 2;
                            break;
                        case 4:
                            lottery.prize = 5;
                            lottery.imgID = 2;
                            break;
                        case 5:
                            lottery.prize = 15;
                            lottery.imgID = 2;
                            break;
                        case 6:
                            lottery.prize = (rd%2 == 0) ? 6 : 14;
                            lottery.imgID = 2;
                            break;
                        case 7:
                            lottery.prize = 13;
                            lottery.imgID = 2;
                            break;
                        case 8:
                            lottery.prize = 4;
                            lottery.imgID = 2;
                            break;
                        case 9:
                            lottery.prize = 3;
                            lottery.imgID = 9;
                            break;
                        case 10:
                            lottery.prize = 2;
                            lottery.imgID = 10;
                            break;
                        case 11:
                            lottery.prize = 12;
                            lottery.imgID = 11;
                            break;
                        case 12:
                            lottery.prize = 0;
                            lottery.imgID = 12;
                            break;
                        case 13:
                            lottery.prize = 8;
                            lottery.imgID = 13;
                            break;
                        case 14:
                            lottery.prize = 11;
                            lottery.imgID = 14;
                            break;
                        case 15:
                            lottery.prize = 7;
                            lottery.imgID = 15;
                            break;
                        case 16:
                            lottery.prize = 1;
                            lottery.imgID = 16;
                            break;
                    }   

                    lottery.speed = 100;
                    roll();
                    click = true;    
                    return false;
                }else{

                }
            }/*function结束*/
        });/*ajax结束*/
    });

};

function GetRandomNum(Min,Max)
{   
    var Range = Max - Min;   
    var Rand = Math.random();   
    return(Min + Math.round(Rand * Range));   
}  

</script>


<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
  var title="名門幸運大轉盤 十萬張代金券等你來搶！";
  var links="http://game.macauos.com/mingmen/index.php?ShareOpenID=<?php echo $_SESSION['OpenID']?>";
  var imgurl="http://game.macauos.com/mingmen/images/m.jpg";
  var desc="嚟潮牛名門麻辣火鍋，品嘗正宗四川風味！";
  wx.config({
    debug: false,
    appId: "<?php echo $signPackage['appId']?>",
    timestamp: <?php echo $signPackage['timestamp']?>,
    nonceStr: "<?php echo $signPackage['nonceStr']?>",
    signature: "<?php echo $signPackage['signature']?>",
    jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone'
        ] // 必填，需要使用的JS接口列表，所有JS接口列表見附錄2
  });

wx.error(function(res){
    //alert(res);
    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

});
  wx.ready(function () {
    // 在這裏調用 API
    wx.onMenuShareTimeline({
        title: desc, // 分享標題
        link: links, // 分享鏈接,將當前登錄用戶轉為puid,以便於發展下線
        imgUrl: imgurl, // 分享圖標
        success: function () { 
            // 用戶確認分享後執行的回調函數
            //alert('分享成功')
            layer.close(shareLayerID);

        },
        cancel: function () { 
            layer.close(shareLayerID);
            // 用戶取消分享後執行的回調函數
        }
    });
    wx.onMenuShareAppMessage({
        title: title, // 分享標題
        desc: desc, // 分享描述
        link: links, // 分享鏈接
        imgUrl: imgurl, // 分享圖標
        type: '', // 分享類型,music、video或link，不填默認為link
        dataUrl: '', // 如果type是music或video，則要提供數據鏈接，默認為空
        success: function () { 
            // 用戶確認分享後執行的回調函數
            //alert('onMenuShareAppMessage');
            layer.close(shareLayerID);
        },
        cancel: function () { 
          layer.close(shareLayerID);
            // 用戶取消分享後執行的回調函數
        }
    });
    wx.onMenuShareQQ({
        title: title, // 分享標題
        desc: desc, // 分享描述
        link: links, // 分享鏈接
        imgUrl: imgurl, // 分享圖標
        success: function () { 
           // 用戶確認分享後執行的回調函數
          layer.close(shareLayerID);
        },
        cancel: function () { 
          layer.close(shareLayerID);
           // 用戶取消分享後執行的回調函數
        }
    });
    wx.onMenuShareWeibo({
        title: title, // 分享標題
        desc: desc, // 分享描述
        link: links, // 分享鏈接
        imgUrl: imgurl, // 分享圖標
        success: function () { 
          layer.close(shareLayerID);
           // 用戶確認分享後執行的回調函數
        },
        cancel: function () { 
          layer.close(shareLayerID);
            // 用戶取消分享後執行的回調函數
        }
    }); 
    wx.onMenuShareQZone({
        title: title, // 分享標題
        desc: desc, // 分享描述
        link: links, // 分享鏈接
        imgUrl: imgurl, // 分享圖標
        success: function () { 
          layer.close(shareLayerID);
           // 用戶確認分享後執行的回調函數
        },
        cancel: function () { 
          layer.close(shareLayerID);
            // 用戶取消分享後執行的回調函數
        }
    });     
  });
</script>
</html>