
var startX = '';
var startY = '';
var endX = '';
var endY = '';
var xDelta = 0;
var boxScroll = 0;
var outBoxId = '';
var contentId = '';

function addScrollListener(obId, cId){
	outBoxId = obId;
	contentId = cId;	
	var domobj = document.getElementById(outBoxId);
	domobj.addEventListener('touchstart',touchStart, false);  
    domobj.addEventListener('touchmove',touchMove, false);  
    domobj.addEventListener('touchend',touchEnd, false);  
}

function removeScrollListener(obId){
	var domobj = document.getElementById(obId);
	domobj.removeEventListener('touchstart',touchStart, false);  
    domobj.removeEventListener('touchmove',touchMove, false);  
    domobj.removeEventListener('touchend',touchEnd, false);  
}

function touchStart(e){  
	var touch = e.touches[0];  
	startX = touch.pageX;  
    startY = touch.pageY;  	 
}

function touchMove(e){  
	e.preventDefault();  
	var touch = e.touches[0];  
	endX = touch.pageX;  
	endY = touch.pageY;	  	
}  

function touchEnd(e){   
	var XLen = startX - endX;
	var YLen = startY - endY;
	
	boxScroll += YLen;
	$('#'+outBoxId).animate({scrollTop: boxScroll},500);
	if($('#'+outBoxId).scrollTop()==0){
		//到顶了
		boxScroll = 0;
	}
	if($('#'+outBoxId).scrollTop()>=$('#'+contentId).height()-$('#'+outBoxId).height()){
		//到底了
		boxScroll -= YLen;
	}
}	
