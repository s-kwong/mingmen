$(function(){
  loadGameTimes();
});

function preventBehavior(e) {
    e.preventDefault(); 
}

function loadGameTimes(){
	var layerLoadTimesIndex = layer.load(0, {shade: [0.3,'#fff'] });	
  $.ajax({
    type: "POST",
    url: "ajax.php?act=UserManage_WeiXinUser_Login",
    dataType: "json",
	  timeout : 10000,
    success:function(data){
      console.log(data);
		  layer.close(layerLoadTimesIndex);
      if(data.IsSuccess == true){
        $('#Mobile').val(data.Object[0].Mobile); 
        if(parseInt(data.Object[0].LotteryCount) <= 0 ){
          $('#times').text("0");
        }else{
          //$('#times').text("0");
          $('#times').text(data.Object[0].LotteryCount);
        }
      }else{
        layer.open({
          type: 1
          ,title: false 
          ,scrollbar:false
          ,closeBtn: true
          ,area: ['318px','302px']
          ,shade: 0.8
          ,offset:'20%'
          ,id: 'LAY_err'
          ,resize: false
          ,skin: 'tipSkin'
          ,btnAlign: 'c'
          ,moveType: 1 //拖拽模式，0或者1
          ,content: '<div class="tipContent"><span class="tipSpan"><img src="images/err.jpg"></span> <div class="closeDiv"><span class="reload">請刷新重試</span><div class="closeBottom"> <div class="closeBtn" id="closeBtn">重试</div></div></div></div>'
          ,success:function(){
            $('#closeBtn').click(function(){window.location.reload();});
          }
        });
	    }
    },
	error: function(XMLHttpRequest, textStatus, errorThrown) {
		layer.close(layerLoadTimesIndex);
    layer.open({
      type: 1
      ,title: false 
      ,scrollbar:false
      ,closeBtn: true
      ,area: ['318px','302px']
      ,shade: 0.8
      ,offset:'20%'
      ,id: 'LAY_err'
      ,resize: false
      ,skin: 'tipSkin'
      ,btnAlign: 'c'
      ,moveType: 1 //拖拽模式，0或者1
      ,content: '<div class="tipContent"><span class="tipSpan"><img src="images/err.jpg"></span> <div class="closeDiv"><span class="reload">請刷新重試</span><div class="closeBottom"> <div class="closeBtn" id="closeBtn">重试</div></div></div></div>'
      ,success:function(){
        $('#closeBtn').click(function(){window.location.reload();});
      }
    });
	}
  });
}

function refLocation(url){
  if(url == '')return;
  window.location.href=url;
}